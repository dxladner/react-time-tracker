/*
  eslint-disable react/prefer-stateless-function, react/jsx-boolean-value,
  no-undef, jsx-a11y/label-has-for, react/jsx-first-prop-new-line
*/
class TimersDashboard extends React.Component {
  state = {
    timers: [],
    startDate: '2017-02-11',
    endDate: '2017-02-11',
    isReportOpen: false,
  };

  componentDidMount() {
    this.loadTimersFromServer();
    setInterval(this.loadTimersFromServer, 5000);
  }

  loadTimersFromServer = () => {
    client.getTimers((serverTimers) => (
        this.setState({ timers: serverTimers })
      )
    );
  };

  handleCreateFormSubmit = (timer) => {
    this.createTimer(timer);
  };

  handleEditFormSubmit = (attrs) => {
    this.updateTimer(attrs);
  };

  handleTrashClick = (timerId) => {
    this.deleteTimer(timerId);
  };

  handleStartClick = (timerId) => {
    this.startTimer(timerId);
  };

  handleStopClick = (timerId) => {
    this.stopTimer(timerId);
  };

  // Inside TimersDashboard
  // ...
  createTimer = (timer) => {
    const t = helpers.newTimer(timer);
    this.setState({
      timers: this.state.timers.concat(t),
    });

    client.createTimer(t);
  };

  updateTimer = (attrs) => {
    this.setState({
      timers: this.state.timers.map((timer) => {
        if (timer.id === attrs.id) {
          return Object.assign({}, timer, {
            title: attrs.title,
            jobnumber: attrs.jobnumber,
          });
        } else {
          return timer;
        }
      }),
    });

    client.updateTimer(attrs);
  };

  deleteTimer = (timerId) => {
    this.setState({
      timers: this.state.timers.filter(t => t.id !== timerId),
    });

    client.deleteTimer(
      { id: timerId }
    );
  };

  startTimer = (timerId) => {
    // ...
    const now = Date.now();

    this.setState({
      timers: this.state.timers.map((timer) => {
        if (timer.id === timerId) {
          return Object.assign({}, timer, {
            runningSince: now,
          });
        } else {
          return timer;
        }
      }),
    });

    client.startTimer(
      { id: timerId, start: now }
    );
  };

  stopTimer = (timerId) => {
    const now = Date.now();

    this.setState({
      timers: this.state.timers.map((timer) => {
        if (timer.id === timerId) {
          const lastElapsed = now - timer.runningSince;
          return Object.assign({}, timer, {
            elapsed: timer.elapsed + lastElapsed,
            runningSince: null,
          });
        } else {
          return timer;
        }
      }),
    });

    client.stopTimer(
      { id: timerId, stop: now }
    );
  };

  render() {
    return (
      <div className="ui grid">

          <div className='eight wide column'>
            <EditableTimerList
              timers={this.state.timers}
              onFormSubmit={this.handleEditFormSubmit}
              onTrashClick={this.handleTrashClick}
              onStartClick={this.handleStartClick}
              onStopClick={this.handleStopClick}
            />
            <ToggleableTimerForm
              onFormSubmit={this.handleCreateFormSubmit}
            />

          </div>
          <div className="eight wide column">
            <Report
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              isReportOpen={this.state.isReportOpen}
              timers={this.state.timers}
            />
          </div>
      </div>
    );
  }
}

class Report extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: this.props.startDate || '',
      endDate: this.props.endDate || '',
      isReportOpen: this.props.isReportOpen || false,
      timers: this.props.timers,
    };
  }

  handleStartDateChange = (e) => {
    this.setState({ startDate: e.target.value });
  //  console.log(this.state.startDate);
  };

  handleEndDateChange = (e) => {
    this.setState({ endDate: e.target.value });
  //  console.log(this.state.endDate);
  };

  handleRRSubmit = (e) => {
    e.preventDefault();
    const startDate = { startDate: this.startDate.value };
    const endDate = { endDate: this.endDate.value };

    this.runReport(startDate, endDate);
  };

  deleteTimer = (timerId) => {
    this.setState({
      timers: this.state.timers.filter(t => t.id !== timerId),
    });

    client.deleteTimer(
      { id: timerId }
    );
  };

  runReport = (startDate, endDate, serverTimers) => {
    this.setState({
     startDate: startDate, endDate: endDate, isReportOpen: true, timers: serverTimers
    });
    this.loadTimersFromServer();
    // client.getReport(
    //   { startDate: startDate, endDate: endDate},
    //   this.setState({ timers: serverTimers })
    // );

  };

  loadTimersFromServer = () => {
    client.getReport((startDate, endDate, serverTimers) => (
        this.setState({ timers: serverTimers })
      )
    );
    console.log(this.state.timers);
  };
  render() {
      return (
        <div className='ui two column centered grid'>
          <div className='column'>
            <h3>Run Weekly Report</h3>
            <form id="weekly_report" className="ui form" onSubmit={this.handleRRSubmit}>
              <div className="field">
              <label>Start Date </label>
                <input
                  type='date'
                  id='startDate'
                  name='startDate'
                  defaultValue={this.state.startDate}
                  ref={(input) => { this.startDate = input } }
                  onChange={this.handleStartDateChange}
                />
              </div>
              <div className="field">
              <label>End Date </label>
                <input
                  type='date'
                  id='endDate'
                  name='endDate'
                  defaultValue={this.state.endDate}
                  ref={(input) => { this.endDate = input } }
                  onChange={this.handleEndDateChange}
                />
              </div>
              <button className="ui primary button">Run Report</button>
            </form>
            <br/>
            <WeeklyReport isReportOpen={this.state.isReportOpen} timers={this.state.timers} />
          </div>
        </div>
      );
  }
}

class WeeklyReport extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isReportOpen: props.isReportOpen,
      timers: props.timers,
    }
  }
  render() {
    let reportItems;
      if (this.props.isReportOpen)
      {
        return(
          <div>
            <h3>Weekly Report</h3>
            <p>Reports NOW</p>
          </div>
        );
      }
      else
      {
        //  reportItems = this.props.timers.map(timer => {
        //     let id = timer.id;
        //     let title = timer.title;
        //     let jobnumber = timer.jobnumber;
        //     let elapsed = timer.elapsed;
        //     let timerStartDate = timer.timerStartDate;
            return(
              <div>
                <h3>Weekly Report</h3>
                <p>No Reports Yet</p>
              </div>
            );
        //});
      }
  }
}
class ToggleableTimerForm extends React.Component {
  state = {
    isOpen: false,
  };

  handleFormOpen = () => {
    this.setState({ isOpen: true });
  };

  handleFormClose = () => {
    this.setState({ isOpen: false });
  };

  handleFormSubmit = (timer) => {
    this.props.onFormSubmit(timer);
    this.setState({ isOpen: false });
  };

  render() {
    if (this.state.isOpen) {
      return (
        <TimerForm
          onFormSubmit={this.handleFormSubmit}
          onFormClose={this.handleFormClose}
        />
      );
    } else {
      return (
        <div className='ui basic content center aligned segment'>
          <button
            className='ui basic button icon'
            onClick={this.handleFormOpen}
          >
            <i className='plus icon' />
          </button>
        </div>
      );
    }
  }
}

class EditableTimerList extends React.Component {
  render() {
    const timers = this.props.timers.map((timer) => (
      <EditableTimer
        key={timer.id}
        id={timer.id}
        title={timer.title}
        jobnumber={timer.jobnumber}
        elapsed={timer.elapsed}
        runningSince={timer.runningSince}
        onFormSubmit={this.props.onFormSubmit}
        onTrashClick={this.props.onTrashClick}
        onStartClick={this.props.onStartClick}
        onStopClick={this.props.onStopClick}
      />
    ));
    return (
      <div id='timers'>
        {timers}
      </div>
    );
  }
}

class EditableTimer extends React.Component {
  state = {
    editFormOpen: false,
  };

  handleEditClick = () => {
    this.openForm();
  };

  handleFormClose = () => {
    this.closeForm();
  };

  handleSubmit = (timer) => {
    this.props.onFormSubmit(timer);
    this.closeForm();
  };

  closeForm = () => {
    this.setState({ editFormOpen: false });
  };

  openForm = () => {
    this.setState({ editFormOpen: true });
  };

  render() {
    if (this.state.editFormOpen) {
      return (
        <TimerForm
          id={this.props.id}
          title={this.props.title}
          jobnumber={this.props.jobnumber}
          onFormSubmit={this.handleSubmit}
          onFormClose={this.handleFormClose}
        />
      );
    } else {
      return (
        <Timer
          id={this.props.id}
          title={this.props.title}
          jobnumber={this.props.jobnumber}
          elapsed={this.props.elapsed}
          runningSince={this.props.runningSince}
          onEditClick={this.handleEditClick}
          onTrashClick={this.props.onTrashClick}
          onStartClick={this.props.onStartClick}
          onStopClick={this.props.onStopClick}
        />
      );
    }
  }
}

class Timer extends React.Component {
  componentDidMount() {
    this.forceUpdateInterval = setInterval(() => this.forceUpdate(), 50);
  }

  componentWillUnmount() {
    clearInterval(this.forceUpdateInterval);
  }

  handleStartClick = () => {
    this.props.onStartClick(this.props.id);
  };

  handleStopClick = () => {
    this.props.onStopClick(this.props.id);
  };

  handleTrashClick = () => {
    this.props.onTrashClick(this.props.id);
  };

  render() {
    const elapsedString = helpers.renderElapsedString(
      this.props.elapsed, this.props.runningSince
    );
    return (
      <div className='ui centered card'>
        <div className='content'>
          <div className='header'>
            {this.props.title}
          </div>
          <div className='meta'>
            {this.props.jobnumber}
          </div>
          <div className='center aligned description'>
            <h2>
              {elapsedString}
            </h2>
          </div>
          <div className='extra content'>
            <span
              className='right floated edit icon'
              onClick={this.props.onEditClick}
            >
              <i className='edit icon' />
            </span>
            <span
              className='right floated trash icon'
              onClick={this.handleTrashClick}
            >
              <i className='trash icon' />
            </span>
          </div>
        </div>
        <TimerActionButton
          timerIsRunning={!!this.props.runningSince}
          onStartClick={this.handleStartClick}
          onStopClick={this.handleStopClick}
        />
      </div>
    );
  }
}

class TimerActionButton extends React.Component {
  render() {
    if (this.props.timerIsRunning) {
      return (
        <div
          className='ui bottom attached red basic button'
          onClick={this.props.onStopClick}
        >
          Stop
        </div>
      );
    } else {
      return (
        <div
          className='ui bottom attached green basic button'
          onClick={this.props.onStartClick}
        >
          Start
        </div>
      );
    }
  }
}

class TimerForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.title || '',
      jobnumber: this.props.jobnumber || '',
    };
  }

  handleTitleChange = (e) => {
    this.setState({ title: e.target.value });
  };

  handleJobNumberChange = (e) => {
    this.setState({ jobnumber: e.target.value });
  };

  handleSubmit = () => {
    this.props.onFormSubmit({
      id: this.props.id,
      title: this.state.title,
      jobnumber: this.state.jobnumber,
    });
  };

  render() {
    const submitText = this.props.id ? 'Update' : 'Create';
    return (
      <div className='ui centered card'>
        <div className='content'>
          <div className='ui form'>
            <div className='field'>
              <label>Title</label>
              <input
                type='text'
                value={this.state.title}
                onChange={this.handleTitleChange}
              />
            </div>
            <div className='field'>
              <label>Job Number</label>
              <input
                type='text'
                value={this.state.jobnumber}
                onChange={this.handleJobNumberChange}
              />
            </div>
            <div className='ui two bottom attached buttons'>
              <button
                className='ui basic blue button'
                onClick={this.handleSubmit}
              >
                {submitText}
              </button>
              <button
                className='ui basic red button'
                onClick={this.props.onFormClose}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <TimersDashboard />,
  document.getElementById('content')
);
